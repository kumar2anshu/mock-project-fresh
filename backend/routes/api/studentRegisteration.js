const express = require('express');
const router = express.Router();
const Student = require('../../models/Student');

//register
router.post('/students', function(req, res, next) {
  Student.create(req.body)
    .then(function(data) {
      res.send(data);
    })
    .catch(next);
});
module.exports = router;
