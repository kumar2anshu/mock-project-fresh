const express = require('express');
const router = express.Router();
const Teacher = require('../../models/Teacher');
const Course = require('../../models/Course');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

//register
router.post('/teachers', function(req, res, next) {
  Teacher.create(req.body)
    .then(function(data) {
      res.send(data);
    })
    .catch(next);
});
module.exports = router;
