const express = require('express');
const router = express.Router();
const Course = require('../../models/Course');

//Add courses

router.post('/courses', function(req, res, next) {
  Course.create(req.body)
    .then(function(data) {
      res.send(data);
    })
    .catch(next);
});

//List of All courses

router.get('/courseavailaible', async (req, res) => {
  try {
    const allcourses = await Course.find({});
    res.status(200).send(allcourses);
  } catch (error) {
    res.status(500).send('Server Error');
  }
});

//delete course
router.delete('/coursedelete/:Id', async (req, res) => {
  try {
    const course = await Course.findByIdAndDelete(req.params.Id);
    res.send(course);
  } catch (error) {
    res.status(500).send('Server Error');
  }
});

//getting particular course on page to update

router.get('/updateById/:Id', async (req, res) => {
  try {
    const tempcourse = await Course.findById(req.params.Id);
    res.send(tempcourse);
  } catch (error) {
    res.status(500).send('Server Error');
  }
});

//Update the course
router.put('/finalUpdate/:id', async (req, res) => {
  try {
    let course = await Course.findById(req.params.id);
    if (!course) return res.status(400).json({ meg: 'Course Not found' });
    await Course.findByIdAndUpdate(req.params.id, {
      $set: {
        courseClass: req.body.courseClass,
        courseName: req.body.courseName,
        mentor: req.body.mentor,
        courseDuration: req.body.courseDuration
      }
    });
  } catch (error) {
    res.status(500).send('Server Error');
  }
});

module.exports = router;
