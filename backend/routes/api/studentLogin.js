const express = require('express');
const router = express.Router();
const Student = require('../../models/Student');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

router.post('/login', (req, res, next) => {
  Student.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(404).json({
          message: 'Authorization is denied ..email not exist'
        });
      }

      bcrypt.compare(req.body.password, user[0].password, (err, result) => {
        if (err) {
          res.status(401).json({
            message: 'Authorization denied'
          });
        }
        if (result) {
          const token = jwt.sign(
            {
              email: user[0].email,
              userId: user[0]._id
            },
            'anshusecret',
            { expiresIn: '1h' }
          );

          res.status(200).send({
            message: 'Authorization successful',
            token: token
          });
        }
        if (!result) {
          res.status(404).json({
            message: 'Password is incorrect'
          });
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

module.exports = router;
