const express = require('express');
const router = express.Router();
const Teacher = require('../models/Teacher');
const Course = require('../models/Course');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

//register
router.post('/teachers', function(req, res, next) {
  Teacher.create(req.body)
    .then(function(data) {
      res.send(data);
    })
    .catch(next);
});

//login

router.post('/login', (req, res, next) => {
  Teacher.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(404).json({
          message: 'Authorization is denied ..email not exist'
        });
      }

      bcrypt.compare(req.body.password, user[0].password, (err, result) => {
        if (err) {
          res.status(401).json({
            message: 'Authorization denied'
          });
        }
        if (result) {
          const token = jwt.sign(
            {
              email: user[0].email,
              userId: user[0]._id
            },
            'anshusecret',
            { expiresIn: '1h' }
          );

          res.status(200).send({
            message: 'Authorization successful',
            token: token
          });
        }
        if (!result) {
          res.status(404).json({
            message: 'Password is incorrect'
          });
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

//Add courses

router.post('/courses', function(req, res, next) {
  Course.create(req.body)
    .then(function(data) {
      res.send(data);
    })
    .catch(next);
});

//List of All courses

router.get('/courseavailaible', async (req, res) => {
  try {
    const allcourses = await Course.find({});
    res.status(200).send(allcourses);
  } catch (error) {
    res.status(500).send('Server Error');
  }
});

//delete course
router.delete('/coursedelete/:Id', async (req, res) => {
  try {
    const course = await Course.findByIdAndDelete(req.params.Id);
    res.send(course);
  } catch (error) {
    res.status(500).send('Server Error');
  }
});

//getting particular course on page to update

router.get('/updateById/:Id', async (req, res) => {
  try {
    const tempcourse = await Course.findById(req.params.Id);
    res.send(tempcourse);
  } catch (error) {
    res.status(500).send('Server Error');
  }
});

//Update the course
router.put('/finalUpdate/:id', async (req, res) => {
  try {
    let course = await Course.findById(req.params.id);
    if (!course) return res.status(400).json({ meg: 'Course Not found' });
    await Course.findByIdAndUpdate(req.params.id, {
      $set: {
        courseClass: req.body.courseClass,
        courseName: req.body.courseName,
        mentor: req.body.mentor,
        courseDuration: req.body.courseDuration
      }
    });
  } catch (error) {
    res.status(500).send('Server Error');
  }
});

module.exports = router;
