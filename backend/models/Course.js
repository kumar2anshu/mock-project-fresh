const mongoose = require('mongoose');

//creating course schema

const courseSchema = new mongoose.Schema({
  courseClass: {
    type: String,
    required: [true, 'Class name is required']
  },
  courseName: {
    type: String,
    required: [true, 'Course name is required']
  },
  courseDuration: {
    type: String,
    default: 'Not Availaible'
  },
  mentor: {
    type: String,
    required: [true, 'Mentor Name is required']
  }
  // article:{
  //   type:String,
  //   required:[true,"article is required"]

  // }
});

const Course = mongoose.model('courses', courseSchema); ///courses will be the  the name of our  collection in database
module.exports = Course;
