const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
let SALT = 10;

//create teacher schema and model
const teacherSchema = new Schema({
  firstName: {
    type: String,
    required: [true, 'Name field is required']
  },
  lastName: {
    type: String
  },
  subject: {
    type: String,
    required: [true, 'subject is required']
  },
  email: {
    type: String,
    required: [true, 'Email is mandatory']
  },
  contact: {
    type: String
  },
  experience: {
    type: String,
    required: [true, 'Experience  is mandatory']
  },
  location: {
    type: String
  },
  password: {
    type: String,
    required: [true, 'password is mandatory']
  }
});
//hashing password
teacherSchema.pre('save', function(next) {
  var user = this;
  if (user.isModified('password')) {
    bcrypt.genSalt(SALT, function(err, salt) {
      if (err) return next(err);
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) return next(err);
        user.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});
//comparing passwords
teacherSchema.methods.comparePassword = function(
  candidatePassword,
  checkpassword
) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) return checkpassword(err);
    checkpassword(null, isMatch);
  });
};

//CREATING MODEL
const Teacher = mongoose.model('teachers', teacherSchema); //teachers is the name of collection
//aLSO  we want object in this collection "teachers" ...that object will be the instance of our schema which we have created above

//finally we want to export this model so that we can use it in outer file...specifically for our route
module.exports = Teacher;
