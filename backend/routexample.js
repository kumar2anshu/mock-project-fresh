const express=require("express");
const app=express();
app.get("/api/courses/:id",(req,res)=>{
    res.send(req.params.id);
});
const port=process.env.port||4000;
app.listen(port,()=>{console.log(`listening on ${port}`)});
