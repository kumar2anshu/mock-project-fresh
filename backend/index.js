//requiring express
const express = require('express');

const bodyParser = require('body-parser'); //we can use modern techniques also

const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);

const cors = require('cors');

//setup express app
const app = express();

//connect to mongo
mongoose.connect('mongodb://localhost/MOCKPROJECT', { useNewUrlParser: true });
//set mongoose promise to global promise bcoz mongoose version of promise has beeen depricated
mongoose.Promise = global.Promise;

app.use(express.static('public')); // middle ware for html file
app.use(
  cors({
    origin: 'http://localhost:3000'
  })
);
app.use(bodyParser.json()); //Now in modern age we can  also use just app.use(express.json({extended:false}))

//importing my own created routes
//define routes

// const routes = require('./routes/api');
// app.use('/api', routes);
app.use('/api', require('./routes/api/teacherRegistration'));
app.use('/api', require('./routes/api/courses'));
app.use('/api', require('./routes/api/teachersLogin'));
app.use('/api', require('./routes/api/studentRegisteration'));

//use is a method to use any middleware

app.use(function(err, req, res, next) {
  console.log(err);
  res.status(422).send({ Error: err.message });
});

const port = process.env.port || 5000;
app.listen(port, () =>
  console.log(` Hi Anshu Kumar, listening on port number: ${port}`)
); //listening request

//we are exporting this for testing.Using this server object we can create our test cases.
module.exports = app;
