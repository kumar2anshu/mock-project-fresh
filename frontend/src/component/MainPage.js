import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route } from 'react-router-dom';
import NavbarComponent from './Navbar';

import Registration from './Registration';
import Home from './Home';
import About from './About';
import Login from './login';
import Course from './Course';
import CourseAvailaible from './CourseAvailaible';
import RegisterStudent from './RegisterStudent';
import ToUpdate from './ToUpdate';
import Logout from './Logout';

class MainPage extends Component {
  render() {
    return (
      <Router>
        <div>
          <NavbarComponent></NavbarComponent>

          <Route exact path='/' component={Home} />
          <Route path='/about' component={About} />

          <Route path='/register' component={Registration} />

          <Route path='/login' component={Login} />
          <Route path='/courses' component={Course} />
          <Route path='/courseavailaible' component={CourseAvailaible} />
          <Route path='/registerstudent' component={RegisterStudent} />
          <Route path='/updatecourse/:Id' component={ToUpdate} />
          <Route path='/logout' component={Logout} />
        </div>
      </Router>
    );
  }
}
export default MainPage;
