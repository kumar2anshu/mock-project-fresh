// import React, { Component } from 'react';
// //import { Jumbotron } from 'reactstrap';
// import axios from 'axios';
// import '../App.css';
// import { Link } from 'react-router-dom';
// import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
// import { Jumbotron, Container } from 'reactstrap';

// //import { Redirect } from 'react-router-dom';

// class RegisterStudent extends Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       firstName: '',
//       lastName: '',
//       email: '',
//       college: '',
//       contact: '',
//       standard: '',

//       location: '',
//       password: '',
//       formErrors: {
//         firstName: '',
//         lastName: '',
//         email: '',
//         college: '',
//         contact: '',
//         standard: '',

//         location: '',
//         password: ''
//       }
//     };
//   }
//   handleChange = e => {
//     this.setState({
//       [e.target.name]: e.target.value
//     });
//   };

// handleSubmit = e => {
//   e.preventDefault();
//   const data = {
//     firstName: this.state.firstName,
//     lastName: this.state.lastName,
//     email: this.state.email,
//     college: this.state.college,
//     contact: this.state.contact,
//     standard: this.state.standard,

//     location: this.state.location,
//     password: this.state.password
//   };

//   axios.post('http://localhost:5000/api/students', data).then(res => {
//     // console.log(res);
//     if (res.statusText === 'OK') {
//       alert('Thank You!! You are successfully Registered');
//       this.props.history.push('/login');
//     }
//   });
// };

//   render() {
//     //debugger
//     const {
//       firstName,
//       lastName,
//       email,
//       college,
//       contact,
//       standard,

//       location,
//       password
//     } = this.state;

//     return (
//       <>
//         <Jumbotron fluid>
//           <Container fluid>
//             <center></center>
//             <center>
//               <Form onSubmit={this.handleSubmit} className='Registerform'>
//                 <div>
//                   <center>
//                     <h1>Registration Form</h1>
//                     <hr className='line' />
//                   </center>
//                 </div>
//                 <FormGroup>
//                   <Label htmlFor='firstnName'>First Name * :</Label>
//                   <Input
//                     type='text'
//                     placeholder='Enter Your First Name'
//                     name='firstName'
//                     value={firstName}
//                     onChange={this.handleChange}
//                   />
//                 </FormGroup>
//                 <FormGroup>
//                   <Label htmlFor='lastName'>Last Name:</Label>
//                   <Input
//                     type='text'
//                     placeholder='Enter Your last name'
//                     name='lastName'
//                     value={lastName}
//                     onChange={this.handleChange}
//                   />
//                 </FormGroup>
//                 <FormGroup>
//                   <Label htmlFor='email'>Email*</Label>
//                   <Input
//                     type='email'
//                     placeholder='Enter Your Email'
//                     value={email}
//                     onChange={this.handleChange}
//                   />
//                 </FormGroup>
//                 <FormGroup>
//                   <Label htmlFor='college'>College</Label>
//                   <Input
//                     type='text'
//                     placeholder='Enter Your College Name'
//                     value={college}
//                     onChange={this.handleChange}
//                   />
//                 </FormGroup>
//                 <FormGroup>
//                   <Label htmlFor='contact'>Contact Number :</Label>
//                   <Input
//                     type='text'
//                     placeholder='Enter Your contact Number'
//                     value={contact}
//                     name='contact'
//                     onChange={this.handleChange}
//                   />
//                 </FormGroup>
//                 <FormGroup>
//                   <Label htmlFor='standard'>Standard</Label>
//                   <Input
//                     type='number'
//                     placeholder='Choose your standard'
//                     min='1'
//                     max='12'
//                     value={standard}
//                     name='standard'
//                     onChange={this.handleChange}
//                   />
//                 </FormGroup>

//                 <FormGroup>
//                   <Label htmlFor=''>Location:</Label>
//                   <Input
//                     type='text'
//                     placeholder='Enter Your Location'
//                     value={location}
//                     onChange={this.handleChange}
//                   />
//                 </FormGroup>
//                 <FormGroup>
//                   <Label htmlFor=''>Create Your Password*</Label>
//                   <Input
//                     type='password'
//                     placeholder='Create Your Password'
//                     value={password}
//                     onChange={this.handleChange}
//                   />
//                 </FormGroup>

//                 <Button>Register</Button>
//               </Form>
// <p>
//   Already have a account? <Link to='/login'>Login </Link>
// </p>
//             </center>
//           </Container>
//         </Jumbotron>
//       </>
//     );
//   }
// }

// export default RegisterStudent;

import React from 'react';
import axios from 'axios';
import '../App.css';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Formik } from 'formik';
import * as EmailValidator from 'email-validator';

const RegisterStudent = () => (
  <Formik
    initialValues={{
      firstName: '',
      lastName: '',
      email: '',
      college: '',
      contact: '',
      standard: '',

      location: '',
      password: ''
    }}
    validate={values => {
      let errors = {};
      const firstNameRegex = /^[a-zA-Z]+$/;
      if (!values.firstName) {
        errors.firstName = 'Required';
      } else if (values.firstName.length < 3) {
        errors.firstName = 'Name should be atleast 3 charcter long';
      } else if (!firstNameRegex.test(values.firstName)) {
        errors.firstName = 'Name should contain only letter';
      }

      if (!values.college) {
        errors.college = 'Required';
      }
      if (!values.standard) {
        errors.standard = 'Required';
      }

      if (!values.email) {
        errors.email = 'Required';
      } else if (!EmailValidator.validate(values.email)) {
        errors.email = 'Invalid email address';
      }

      const passwordRegex = /(?=.*[0-9])/;
      if (!values.password) {
        errors.password = 'Required';
      } else if (values.password.length < 8) {
        errors.password = 'Password must be 8 characters long.';
      } else if (!passwordRegex.test(values.password)) {
        errors.password = 'Invalida password. Must contain one number';
      }

      const contactRegex = /^\d{10}$/;
      if (!values.contact) {
        errors.contact = 'Mobile No is Required';
      } else if (values.contact.length < 10) {
        errors.contact = 'Mobile number must be of 10 characters long';
      } else if (!contactRegex.test(values.contact)) {
        errors.contact =
          'Invalid Mobile Number.  Mobile No should be the number of 10 digits';
      }

      return errors;
    }}
  >
    {props => {
      const {
        values,
        touched,
        errors,

        handleChange,
        handleBlur,
        handleSubmit
      } = props;
      return (
        <Form onSubmit={handleSubmit} className='Registerform'>
          <div>
            <center>
              <h1>Registration Form</h1>
              <hr className='line' />
            </center>
          </div>
          <FormGroup>
            <Label htmlFor='firstnName'>First Name * :</Label>
            <Input
              type='text'
              placeholder='Enter Your First Name'
              name='firstName'
              value={values.firstName}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.firstName && touched.firstName && 'error'}
            />
            {errors.firstName && touched.firstName && (
              <div className='input-feedback'>{errors.firstName}</div>
            )}
          </FormGroup>
          <FormGroup>
            <Label htmlFor='lastName'>Last Name:</Label>
            <Input
              type='text'
              placeholder='Enter Your last name'
              name='lastName'
              value={values.lastName}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.lastName && touched.lastName && 'error'}
            />
            {errors.lastName && touched.lastName && (
              <div className='input-feedback'>{errors.lastName}</div>
            )}
          </FormGroup>
          <FormGroup>
            <Label htmlFor='email'>Email*</Label>
            <Input
              type='email'
              placeholder='Enter Your Email'
              name='email'
              value={values.email}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.email && touched.email && 'error'}
            />
            {errors.email && touched.email && (
              <div className='input-feedback'>{errors.email}</div>
            )}
          </FormGroup>
          <FormGroup>
            <Label htmlFor='college'>College*</Label>
            <Input
              type='text'
              placeholder='Enter Your College Name'
              value={values.college}
              name='college'
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.college && touched.college && 'error'}
            />
            {errors.college && touched.college && (
              <div className='input-feedback'>{errors.college}</div>
            )}
          </FormGroup>
          <FormGroup>
            <Label htmlFor='contact'>Contact Number* :</Label>
            <Input
              type='text'
              placeholder='Enter Your contact Number'
              value={values.contact}
              name='contact'
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.contact && touched.contact && 'error'}
            />
            {errors.contact && touched.contact && (
              <div className='input-feedback'>{errors.contact}</div>
            )}
          </FormGroup>
          <FormGroup>
            <Label htmlFor='standard'>Standard*</Label>
            <Input
              type='number'
              placeholder='Choose your standard'
              min='1'
              max='12'
              value={values.standard}
              name='standard'
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.standard && touched.standard && 'error'}
            />
            {errors.standard && touched.standard && (
              <div className='input-feedback'>{errors.standard}</div>
            )}
          </FormGroup>

          <FormGroup>
            <Label htmlFor=''>Location:</Label>
            <Input
              type='text'
              placeholder='Enter Your Location'
              value={values.location}
              name='location'
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.location && touched.location && 'error'}
            />
            {errors.location && touched.location && (
              <div className='input-feedback'>{errors.location}</div>
            )}
          </FormGroup>
          <FormGroup>
            <Label htmlFor=''>Password*</Label>
            <Input
              type='password'
              placeholder='Create Your Password'
              value={values.password}
              name='password'
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.password && touched.password && 'error'}
            />
            {errors.password && touched.password && (
              <div className='input-feedback'>{errors.password}</div>
            )}
          </FormGroup>
          <center>
            <Button>Register</Button>

            <p>
              Already have an account? <Link to='/login'>Login </Link>
            </p>
          </center>
        </Form>
      );
    }}
  </Formik>
);

export default RegisterStudent;
