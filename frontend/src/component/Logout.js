import React, { Component } from 'react';

class Logout extends Component {
  constructor(props) {
    super(props);
    sessionStorage.clear();
    // window.location.reload();

    this.state = {};
  }
  componentDidMount() {
    this.props.history.push('/');
    window.location.reload();
  }

  render() {
    return (
      <div>
        <h1>Logged out</h1>

        <br />
      </div>
    );
  }
}

export default Logout;
