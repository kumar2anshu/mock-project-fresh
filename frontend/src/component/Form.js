import React from 'react';
import { useState } from 'react';
import useForm from './useForm';

const Form = () => {
  debugger;
  const { handleChange, handleSubmit, values } = useForm(submit);

  function submit() {
    debugger;
    console.log('Submitted');
  }

  return (
    <div>
      <form noValidate onSubmit={handleSubmit}>
        <div>
          <label htmlFor='email'>Email</label>
          <input
            name='email'
            type='email'
            value={values.email}
            onChange={handleChange}
          />
          {/*error messages here*/}
        </div>
        <div>
          <label htmlFor='password'>Password</label>
          <input
            name='password'
            type='password'
            value={values.password}
            onChange={handleChange}
          />
        </div>
        <button type='submit'>Login</button>
      </form>
    </div>
  );
};

export default Form;
