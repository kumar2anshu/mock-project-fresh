import React, { Component } from 'react';
import axios from 'axios';
import '../App.css';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

class Registration extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      contact: '',
      experience: '',
      subject: '',
      location: '',
      password: ''
    };
  }
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const data = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      contact: this.state.contact,
      experience: this.state.experience,
      subject: this.state.subject,
      location: this.state.location,
      password: this.state.password
    };
    //console.log(process.env.serverAddress);

    axios.post(`http://localhost:5000/api/teachers`, data).then(res => {
      // console.log(res);
      if (res.statusText === 'OK') {
        alert('Thank You!! You are successfully Registered');
        this.props.history.push('/login');
      }
    });
  };

  render() {
    const {
      firstName,
      lastName,
      email,
      contact,
      experience,
      subject,
      location,
      password
    } = this.state;
    return (
      <>
        <center></center>
        <center>
          <Form onSubmit={this.handleSubmit} className='Registerform'>
            <div>
              <center>
                <h1>Registration Form</h1>
                <hr className='line' />
              </center>
            </div>
            <FormGroup>
              <Label htmlFor='firstnName'>First Name * :</Label>
              <Input
                type='text'
                placeholder='Enter Your First Name'
                name='firstName'
                value={firstName}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor='lastName'>Last Name:</Label>
              <Input
                type='text'
                placeholder='Enter Your last name'
                name='lastName'
                value={lastName}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor='email'>Email*</Label>
              <Input
                type='email'
                name='email'
                placeholder='Enter Your Email'
                value={email}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor='contact'>Contact Number :</Label>
              <Input
                type='text'
                placeholder='Enter Your contact Number'
                value={contact}
                name='contact'
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor='experience'>Experience*</Label>
              <Input
                type='number'
                placeholder='Choose your experience'
                min='0'
                max='40'
                value={experience}
                name='experience'
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor='subject'>Subject*:</Label>
              <select
                name='subject'
                id=''
                value={subject}
                onChange={this.handleChange}
              >
                <option value='physics'>Physics</option>
                <option value='chemistry'>Chemistry</option>
                <option value='mathematics'>Mathematics</option>
              </select>
            </FormGroup>
            <br />
            <FormGroup>
              <Label htmlFor=''>Location:</Label>
              <Input
                type='text'
                name='location'
                placeholder='Enter Your Location'
                value={location}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor=''>Password*</Label>
              <Input
                type='password'
                name='password'
                placeholder='Create Your Password'
                value={password}
                onChange={this.handleChange}
              />
            </FormGroup>

            <Button>Register</Button>
          </Form>
          <p>
            Already have a account? <Link to='/login'>Login </Link>
          </p>
        </center>
      </>
    );
  }
}

export default Registration;
