import React from 'react';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';
import '../App.css';

class NavbarComponent extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    if (sessionStorage.getItem('loginStatus') === 'Yes') {
      return (
        <div>
          <Navbar light expand='lg' bg='dark' variant='dark' color='info'>
            <NavbarBrand href='/'>
              <i className='fas fa-book-reader'>FreeEducationAdda</i>
            </NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className='ml-auto' navbar>
                <NavItem>
                  <Link className='list' to='/about'>
                    About us
                  </Link>
                </NavItem>
                <NavItem>
                  <Link className='list' to='/logout'>
                    <i class='fas fa-sign-out-alt'></i> Logout
                  </Link>
                </NavItem>

                <NavItem>
                  <Link className='list' to='/courseavailaible'>
                    Courses
                  </Link>
                </NavItem>

                <NavItem>
                  <Link className='list' to='/courses'>
                    {' '}
                    Add Course
                  </Link>
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>
        </div>
      );
    } else {
      return (
        <div>
          <Navbar light expand='lg' bg='dark' variant='dark' color='info'>
            <NavbarBrand href='/'>
              <i className='fas fa-book-reader'>FreeEducationAdda</i>
            </NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className='ml-auto' navbar>
                <NavItem>
                  <Link className='list' to='/about'>
                    About us
                  </Link>
                </NavItem>

                <NavItem>
                  <Link className='list' to='/login'>
                    <i class='fas fa-sign-in-alt'></i> Login
                  </Link>
                </NavItem>

                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle className='register'>Register</DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem>
                      <Link to='/registerstudent'>Student</Link>
                    </DropdownItem>
                    <DropdownItem>
                      <Link to='/register'>Teacher</Link>
                    </DropdownItem>
                    <DropdownItem divider />
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
            </Collapse>
          </Navbar>
        </div>
      );
    }
  }
}
export default NavbarComponent;
