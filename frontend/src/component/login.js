import React from 'react';
import axios from 'axios';
//import '../App.css';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }
  //debugger
  render() {
    const { email, password } = this.state;
    return (
      <center>
        <Form onSubmit={this.handleSubmit} className='loginForm'>
          <div>
            <center>
              <h2>Enter Your Login Credential</h2>
              <hr className='line' />
            </center>
          </div>
          <FormGroup>
            <Label htmlFor='email'>Email</Label>
            <Input
              name='email'
              type='text'
              placeholder='Enter your email'
              value={email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup>
            <Label htmlFor='email'>Password</Label>
            <Input
              name='password'
              type='password'
              placeholder='Enter your password'
              value={password}
              onChange={this.handleChange}
            />
          </FormGroup>
          <Button>Login</Button>
        </Form>
        <p>
          Don't have account? <Link to='/register'>Register</Link>
        </p>
      </center>
    );
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const data = {
      email: this.state.email,
      password: this.state.password
    };

    axios.post('http://localhost:5000/api/login', data).then(res => {
      console.log(res);
      alert(res.data.message);
      sessionStorage.setItem('loginStatus', 'Yes');
      this.props.history.push('/');
      window.location.reload();
    });
  };
}

export default Login;
