import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import {
  Card,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button
} from 'reactstrap';

class CourseCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Id: '',
      isbuttonClicked: false
    };
    console.log(this.props);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }
  handleDelete = Id => {
    axios.delete(`http://localhost:5000/api/coursedelete/${Id}`).then(res => {
      console.log(res);
      window.location.reload();
    });
    //alert(`Course id of this course is ${Id} `);
    //console.log(this.state.courses);
  };

  handleUpdate = Id => {
    alert(Id);
    this.setState({
      Id: Id,
      isbuttonClicked: true
    });
  };

  render() {
    if (this.state.isbuttonClicked) {
      return <Redirect to={`/updatecourse/${this.state.Id}`} />;
    }

    return (
      <Card>
        <CardBody>
          <CardTitle>
            <h5>Course Name:</h5>
            {this.props.course.courseName}
          </CardTitle>
          <CardSubtitle>
            <h5>Mentor:</h5>
            {this.props.course.mentor}
          </CardSubtitle>
          <CardSubtitle>
            <h5>Duration:</h5>
            {this.props.course.courseDuration} Month
          </CardSubtitle>
          <CardSubtitle>
            <h5>Class</h5>
            {this.props.course.courseClass}
          </CardSubtitle>
        </CardBody>
        <CardBody>
          <CardText>Free Education Adda</CardText>
        </CardBody>
        <Button onClick={() => this.handleDelete(this.props.course._id)}>
          Delete
        </Button>
        <br />
        <Button onClick={() => this.handleUpdate(this.props.course._id)}>
          Update
        </Button>
      </Card>
    );
  }
}

export default CourseCard;
