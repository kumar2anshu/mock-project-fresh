import React, { Component } from 'react';
import axios from 'axios';
import '../App.css';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Jumbotron, Container } from 'reactstrap';
import { Formik } from 'formik';
import * as EmailValidator from 'email-validator';

const validatedRegisterStudent = () => {
  <Formik
    initialValues={{
      firstName: '',
      lastName: '',
      email: '',
      college: '',
      contact: '',
      standard: '',

      location: '',
      password: ''
    }}
    onSubmit={(values, { setSubmitting }) => {
      console.log('registering');
    }}
    validate={values => {
      let errors = {};
      if (!values.firstName) {
        errors.firstName = 'Required';
      }

      if (!values.college) {
        errors.college = 'Required';
      }
      if (!values.standard) {
        errors.standard = 'Required';
      }

      if (!values.email) {
        errors.email = 'Required';
      } else if (!EmailValidator.validate(values.email)) {
        errors.email = 'Invalid email address';
      }

      const passwordRegex = /(?=.*[0-9])/;
      if (!values.password) {
        errors.password = 'Required';
      } else if (values.password.length < 8) {
        errors.password = 'Password must be 8 characters long.';
      } else if (!passwordRegex.test(values.password)) {
        errors.password = 'Invalida password. Must contain one number';
      }

      return errors;
    }}
  >
    {props => {
      const {
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        handleBlur,
        handleSubmit
      } = props;
      return (
        <Form onSubmit={handleSubmit} className='Registerform'>
          <div>
            <center>
              <h1>Registration Form</h1>
              <hr className='line' />
            </center>
          </div>
          <FormGroup>
            <Label htmlFor='firstnName'>First Name * :</Label>
            <Input
              type='text'
              placeholder='Enter Your First Name'
              name='firstName'
              value={values.firstName}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.firstName && touched.firstName && 'error'}
            />
            {errors.firstName && touched.firstName && (
              <div className='input-feedback'>{errors.firstName}</div>
            )}
          </FormGroup>
          <FormGroup>
            <Label htmlFor='lastName'>Last Name:</Label>
            <Input
              type='text'
              placeholder='Enter Your last name'
              name='lastName'
              value={values.lastName}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.lastName && touched.lastName && 'error'}
            />
            {errors.lastName && touched.lastName && (
              <div className='input-feedback'>{errors.lastName}</div>
            )}
          </FormGroup>
          <FormGroup>
            <Label htmlFor='email'>Email*</Label>
            <Input
              type='email'
              placeholder='Enter Your Email'
              value={values.email}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.email && touched.email && 'error'}
            />
            {errors.email && touched.email && (
              <div className='input-feedback'>{errors.email}</div>
            )}
          </FormGroup>
          <FormGroup>
            <Label htmlFor='college'>College</Label>
            <Input
              type='text'
              placeholder='Enter Your College Name'
              value={values.college}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.college && touched.college && 'error'}
            />
            {errors.college && touched.college && (
              <div className='input-feedback'>{errors.college}</div>
            )}
          </FormGroup>
          <FormGroup>
            <Label htmlFor='contact'>Contact Number :</Label>
            <Input
              type='text'
              placeholder='Enter Your contact Number'
              value={values.contact}
              name='contact'
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.college && touched.college && 'error'}
            />
            {errors.contact && touched.contact && (
              <div className='input-feedback'>{errors.contact}</div>
            )}
          </FormGroup>
          <FormGroup>
            <Label htmlFor='standard'>Standard</Label>
            <Input
              type='number'
              placeholder='Choose your standard'
              min='1'
              max='12'
              value={values.standard}
              name='standard'
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.standard && touched.standard && 'error'}
            />
            {errors.standard && touched.standard && (
              <div className='input-feedback'>{errors.contact}</div>
            )}
          </FormGroup>

          <FormGroup>
            <Label htmlFor=''>Location:</Label>
            <Input
              type='text'
              placeholder='Enter Your Location'
              value={values.location}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.location && touched.location && 'error'}
            />
            {errors.location && touched.location && (
              <div className='input-feedback'>{errors.location}</div>
            )}
          </FormGroup>
          <FormGroup>
            <Label htmlFor=''>Create Your Password*</Label>
            <Input
              type='password'
              placeholder='Create Your Password'
              value={values.password}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.password && touched.password && 'error'}
            />
            {errors.password && touched.password && (
              <div className='input-feedback'>{errors.password}</div>
            )}
          </FormGroup>
        </Form>
      );
    }}
  </Formik>;
};
