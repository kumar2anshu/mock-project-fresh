import { useState } from 'react';

const useForm = call => {
  const [values, setValues] = useState({ email: '', password: '' });
  const handleChange = event => {
    const { name, value } = event.target;

    setValues({
      ...values,
      [name]: value
    });
  };
  const handleSubmit = event => {
    event.preventDefault();
    call();
  };

  return {
    handleChange,
    handleSubmit,
    values
  };
};
export default useForm;
