import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';

// const dotenv = require('dotenv');
// dotenv.config();

ReactDOM.render(<App />, document.getElementById('root'));
